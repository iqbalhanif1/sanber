from pymongo import MongoClient

class database:
    def __init__(self):
        try:
            self.nosql_db = MongoClient()
            self.db = self.nosql_db.perpustakaan
            self.mongo_col = self.db.books
            print("database connected")
        except Exception as e:
            print(e)
    
    def showBooks(self):
        result = self.mongo_col.find()
        return result
    
    def showBookById(self):
        query = {"_id": {ObjectId(key)}}
        result = mongo_col.find(query)
        return result
    
    def searchBookByName(self, key):
        query = {"nama" : {"$regex": key, "$options": "i"}}
        result = self.mongo_col.find(query)
        return result
    
    def insertBook(self,document):
        self.mongo_col.insert_one(document)
    
    def updateBookById(self):
        query = {"_id": {ObjectId(key)}}
        values = {"$set": { "_id": {ObjectId(value)}}
        self.mongo_col.update(query, values)
    
    def deleteBookById(self):
        query ={"_id": {ObjectId(key)}}
        self.mongo_col.delete(query)